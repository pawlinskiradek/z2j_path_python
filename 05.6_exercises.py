# 1. Print the result of the calculation 3 ** .125 as a ﬁxed-point number with three decimal places.
calculation = 3 ** .125
print(f"The result of calculation 3 ** .125 is {calculation:.2f}.")
# 2. Print the number 150000 as currency, with the thousands grouped with commas. Currency should be displayed with two decimal places.
currency = 150000
print(f"The currency is {currency:,.2f}")
# 3. Print the result of 2 / 10 as a percentage with no decimal places. The output should look like 20%.
result = 2 / 10
print(f"This is {result:.0%}")