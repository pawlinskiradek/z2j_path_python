"""
1. Write a script that repeatedly asks the user to input an integer,
displaying a message to “try again” by catching the ValueError that
is raised if the user did not enter an integer.
Once the user enters an integer, the program should display
the number back to the user and end without crashing.
2. Write a program that asks the user to input a string and an integer
n. Then display the character at index n in the string.
Use error handling to make sure the program doesn’t crash
if the user does not enter an integer or the index is out of bounds.
The program should display a diﬀerent message depending on
what error occurs.
"""

while True:
    try:
        number = int(input("Enter an integer: "))
        break
    except ValueError:
        print("try again")
print(number)

# or
"""
while True:
    try:
        number = input("Enter an integer: ")
        print(int(number))
        break
    except ValueError:
        print("try again")
"""

while True:
    try:
        word = input("Enter a string: ")
        number = int(input("Enter an integer: "))
        print(f"character at index {number} in the {word} is {word[number]}")
        break
    except ValueError:
        print("it is not an integer, try again")
    except IndexError:
        print("the index is out of bounds")

