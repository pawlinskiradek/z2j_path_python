# Challenge: Capital City Loop
# Review your state capitals along with dictionaries and while loops!

# First, ﬁnish ﬁlling out the following dictionary with the remaining
# states and their associated capitals in a ﬁle called capitals.py.

# Next, pick a random state name from the dictionary, and assign both
# the state and it’s capital to two variables. You’ll need to import the
# random module at the top of your program.
# Then display the name of the state to the user and ask them to enter
# the capital. If the user answers, incorrectly, repeatedly ask them for
# the capital name until they either enter the correct answer or type the
# word “exit”.

# If the user answers correctly, display "Correct" and end the program.
# However, if the user exits without guessing correctly, display the cor-
# rect answer and the word "Goodbye".
import random

capitals_dict = {
    "Alabama": "Montgomery",
    "Alaska": "Juneau",
    "Arizona": "Phoenix",
    "Arkansas": "Little Rock",
    "California": "Sacramento",
    "Colorado": "Denver",
    "Connecticut": "Hartford",
    "Delaware": "Dover",
    "Florida": "Tallahassee",
    "Georgia": "Atlanta",
    "Hawaii": "Honolulu",
    "Idaho": "Boise",
    "Illinois": "Springfield",
    "Indiana": "Indianapolis",
    "Iowa": "Des Moines",
    "Kansas": "Topeka",
    "Kentucky": "Frankfort",
    "Louisiana": "Baton Rouge",
    "Maine": "Augusta",
    "Maryland": "Annapolis",
    "Massachusetts": "Boston",
    "Michigan": "Lansing",
    "Minnesota": "Saint Paul",
    "Mississippi": "Jackson",
    "Missouri": "Jefferson City",
    "Montana": "Helena",
    "Nebraska": "Lincoln",
    "Nevada": "Carson City",
    "New Hampshire": "Concord",
    "New Jersey": "Trenton",
    "New Mexico": "Santa Fe",
    "New York": "Albany",
    "North Carolina": "Raleigh",
    "North Dakota": "Bismarck",
    "Ohio": "Columbus",
    "Oklahoma": "Oklahoma City",
    "Oregon": "Salem",
    "Pennsylvania": "Harrisburg",
    "Rhode Island": "Providence",
    "South Carolina": "Columbia",
    "South Dakota": "Pierre",
    "Tennessee": "Nashville",
    "Texas": "Austin",
    "Utah": "Salt Lake City",
    "Vermont": "Montpelier",
    "Virginia": "Richmond",
    "Washington": "Olympia",
    "West Virginia": "Charleston",
    "Wisconsin": "Madison",
    "Wyoming": "Cheyenne",
}

def capital_check(capitals):
    state = random.choice(list(capitals))
    capital = capitals[state]
    answer = ""
    while answer.capitalize() != "Exit":  # first letter to uppercase
        answer = input(f"Enter the capital of state {state}, or type 'exit' to end the program: ")
        # if answer.capitalize() == "Exit":
        #     continue
        if answer.capitalize() == capital:
            print("Correct")
            break
    print(f"The capital of state {state} is {capital}.")

capital_check(capitals_dict)

# *****************************************************************
# second solution

# # Pull random state and capital pair from the dict by casting to list of tuples
# state, capital = random.choice(list(capitals_dict.items()))

# # Game loop continues until the user inputs "exit"
# # or guesses the correct capital
# while True:
#     guess = input(f"What is the capital of '{state}'? ").lower()
#     if guess == "exit":
#         print(f"The capital of '{state}' is '{capital}'.")
#         print("Goodbye")
#         break
#     elif guess == capital.lower():
#         print("Correct! Nice job.")
#         break