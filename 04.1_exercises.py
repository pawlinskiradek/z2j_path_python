# 1. Print a string that uses double quotation marks inside the string.
print('Powiedziałem do Jacka: "Możesz już iść!"')
# 2. Print a string that uses an apostrophe inside the string.
print("I'm Radek!")
# 3. Print a string that spans multiple lines, with whitespace preserved.
print("""Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Morbi dignissim enim turpis, a sodales nisl pretium sed. 
          Praesent sodales, urna ut faucibus euismod, felis ante mollis 
      nisi, vitae consequat mi massa et risus. Etiam sed lectus 
      lacus. Vestibulum vitae feugiat mi. Vivamus sed faucibus nisl, 
           ut venenatis tortor. Integer luctus quam a porta eleifend. 
        Donec suscipit aliquam ligula, quis pretium lectus porttitor 
      vitae. Etiam fermentum, justo id sollicitudin convallis, risus 
           ante maximus orci, eget tempor tortor enim hendrerit lorem. 
      Nulla efficitur sollicitudin ante sit amet porttitor. In ligula 
         tellus, congue quis nunc at, tincidunt tristique metus. Donec 
      id imperdiet turpis. Ut id lectus arcu.""")
# 4. Print a string that is coded on multiple lines but displays on a single line.
print("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi \
dignissim enim turpis, a sodales nisl pretium sed. Praesnt \
sodales, urna ut faucibus euismod, felis ante mollis nisi, \
vitae consequat mi massa et risus. Etiam sed lectus lacus. \
Vestibulum vitae feugiat mi. Vivamus sed faucibus nisl, ut \
venenatis tortor. Integer luctus quam a porta eleifend. Donec \
suscipit aliquam ligula, quis pretium lectus porttitor vitae. \
Etiam fermentum, justo id sollicitudin convallis, risus ante \
maximus orci, eget tempor tortor enim hendrerit lorem. Nulla \
efficitur sollicitudin ante sit amet porttitor. In ligula \
tellus, congue quis nunc at, tincidunt tristique metus. Donec \
id imperdiet turpis. Ut id lectus arcu.")