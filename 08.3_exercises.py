# 1. Write a script that prompts the user to enter a word using the
# input() function, stores that input in a variable, and then displays
# whether the length of that string is less than 5 characters, greater
# than 5 characters, or equal to 5 characters by using a set of if, elif
# and else statements.

word = input("Enter a word: ")
word_len = len(word)

if word_len < 5:
    print("The length of that word is less then 5 characters.")
elif word_len > 5:
    print("The length of that word is greater then 5 characters.")
else:
    print("The length of that word is equal to 5 characters.")