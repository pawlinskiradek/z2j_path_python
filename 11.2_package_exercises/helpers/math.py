# math.py

"""
In the math.py module, as a function called area() that takes two parameters called length and width and returns their product length * width.
"""

def area(length, width):
    return length * width