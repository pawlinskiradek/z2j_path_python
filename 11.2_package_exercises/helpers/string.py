# string.py
"""
In the string.py module, add a function called shout() that takes a single string parameter and returns a new string with all of the letters in uppercase.
"""
def shout(name):
    return name.upper()