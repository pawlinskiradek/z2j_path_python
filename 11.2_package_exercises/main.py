# main.py

"""
In the root project folder, create a module called main.py that imports the shout() and area() functions. Use the shout() and area() functions to print the following output:
THE AREA OF A 5-BY-8 RECTANGLE IS 40
"""

from helpers import math, string

x = 5
y = 8
area = math.area(x, y)
print(string.shout(f"The area of a {x}-by-{y} rectangle is {area}"))