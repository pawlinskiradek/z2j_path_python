# Challenge: Move All Image Files To a New Directory

# In the Chapter 12 Practice Files folder, there is a subfolder called practice/. The directory contains several ﬁles and subfolder. Some of the ﬁles are images ending with either the .png, .gif, or the .jpg ﬁle extension.
# Create a new folder in the Practice Files folder called images/ and move all image ﬁles to that folder. When you are done, the new folder should have four ﬁles in it:
# 1. image1.png
# 2. image2.gif
# 3. image3.png
# 4. image4.jpg

from pathlib import Path

# path to practice_files/ directory
practice = Path.cwd() / "practice_files"

# path to images/ new directory
images = practice / "images"
# create images/ directory if needed and no exception is rised
images.mkdir(parents=True, exist_ok=True)

# create lists of paths to files in practice_files/ directory and every subdirectory with specific extension
png = list(practice.rglob("*.png"))
gif = list(practice.rglob("*.gif"))
jpg = list(practice.rglob("*.jpg"))

# replace file from list to new destination
for file in png:
    file.replace(images / file.name)

for file in gif:
    file.replace(images / file.name)

for file in jpg:
    file.replace(images / file.name)

# # **********************************************************
# # Other method
# # Search for image files in the documents directory and move
# # them to the images/ directory
# for path in practice.rglob("*.*"):
#     if path.suffix.lower() in [".png", ".jpg", ".gif"]:
#         path.replace(images / path.name)
# # **********************************************************

# print all files in images/ directory
for file in list(images.glob("*.*")):
    print(file.name)
    