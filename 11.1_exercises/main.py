# main.py

# 2. Create a module called main.py that imports the greet() function from greet.py and calls the function with the argument "Real Python".

from greeter import greet

greet("Real Python")