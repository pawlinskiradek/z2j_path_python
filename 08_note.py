# Conditional Logic and Control Flow

# Compare Values
type(True)  # <class 'bool'> 1 == 1
type(False)  # <class 'bool'> 3 > 5

# Ad some Logic
1 < 2 and 3 < 4  # Both are True = True
2 < 1 and 4 < 3  # Both are False = False
1 < 2 and 4 < 3  # Second statement is False = False
2 < 1 and 3 < 4  # First statement is False = False

1 < 2 or 3 < 4  # Both are True = True
2 < 1 or 4 < 3  # Both are False = False
1 < 2 or 4 < 3  # Second statement is False = True
2 < 1 or 3 < 4  # First statement is False = True

not True  # False
not False  # True

not True == False  # True
False == not True  # SyntaxError: invalid syntax
# Operator Order of Precedence (Highest to Lowest):
# <, <=, ==, >=, >
# not
# and
# or
False == (not True)  # True

# The if Statement
grade = 85

if grade >= 90:
    print("You passed the class with a A.")
# The elif Keyword
elif grade >= 80:
    print("You passed the class with a B.")
elif grade >= 70:
    print("You passed the class with a C.")
# The else Keyword
else:
    print("You did not pass the class :(")

print("Thank you for attending.")

# Nested if Statement

# Compound conditional expression

# 8.4 Challenge: Find the Factors of a Number
# A factor of a positive integer n is any positive integer less than or equal
# to n that divides n with no remainder.
# For example, 3 is a factor of 12 because 12 divided by 3 is 4, with no
# remainder. However, 5

# if Statement and for Loops
sum_of_enens = 0

for n in range(1, 100):
    if n % 2 == 0:
        sum_of_enens = sum_of_enens + n

print(sum_of_enens)

# break - out of the Loop
for n in range(0, 4):
    if n == 2:
        break
    print(n)

print(f"Finished with n = {n}")  # output: 2

# continue - skip any remaining code in the loop body and continue on to the next iteration
for i in range(0, 4):
    if i == 2:
        continue
    print(i)
    
print(f"Finished with i = {i}")  # output: 3

# for...else Loops
phrase = "it marks the spot"

for character in phrase:
    if character == "X":
        print(" X ")
        break  # out of for loop
else:
    print("There was no 'X' in the phrase")

# Exceptions
# The try and except Keywords
try:
    number = int(input("Enter an integer: "))
except ValueError:
    print("Tha wan not an integer")
    
# multiple exception types
def divide(num1, num2):
    try:
        print(num1 / num2)
    except (TypeError, ZeroDivisionError):
        print("encountered a problem")
# or
    except TypeError:
        print("Both arguments must be numbers")
    except ZeroDivisionError:
        print("num2 must not be 0")

# The random module
import random
random.randint(1, 10)

from random import randint
randint(1, 10)




# strona 229