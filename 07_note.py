# Finding and Fixing Code Bugs
    
def add_underscores(word):
    new_word = "_"
    for i in range(0, len(word)):
        new_word = new_word + word[i] + "_"
    return new_word

phrase = "hello"
print(add_underscores(phrase))

# Step 1: Make a Guess About Where the Bug Is Located

# Step 2: Set a Breakpoint and Inspect the Code

# Step 3: Identify the Error and Attempt to Fix It

# Step 4: Repeat Steps 1–3 Until the Bug is Gone

# Alternative Ways to Find Mistakes in Your Code
# using well placed print() functions to display the values of your variables.

def add_underscores(word):
    new_word = "_"
    for i in range(0, len(word)):
        new_word = word[i] + "_"
        print(f"i - {i}; new_word = {new_word}")
    return new_word

phrase = "hello"
print(add_underscores(phrase))

# The process of re-writing existing code to be cleaner, easier to read and understand, or adhere to code standards set by a team is called REFACTORING.

def add_underscores(word):
    new_word = "_"
    for char in word:
        new_word = new_word + char + "_"
    return new_word



# strona 185