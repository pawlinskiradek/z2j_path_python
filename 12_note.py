# File Input and Output

# Working With File Paths in Python
import pathlib

# Creating Path Objects
# 1. From a string
path = pathlib.Path("/Users/David/Documents/hello.txt")  # macOs file path
path = pathlib.Path("C:\Users\David\Desktop\hello.txt")  # Windows file path raises an exeption SyntaxError: (unicode error)# To get around thi problem:
# - use a foward slash (/) in Windows file path:
# path = pathlib.Path("C:/Users/David/Desktop/hello.txt")
# # - turn string into raw string:
path = pathlib.Path(r"C:\Users\David\Desktop\hello.txt")  # ignore any escape sequences and just read the string as-is

# 2. With Path.home() and Path.cwd() class methods
"""
Path.home() - home directory:
- Windows: C:\Users\<username- macOS: /Users/<username- Ubuntu Linux: /home/<username"""
home = pathlib.Path.home()
home  # output Windows: WindowsPath("C:\\Users\\David")
# output othes OS: PosixPath('/home/David'), PosixPath('/Users/David')
"""
Path.cwd() - current working directory
"""
pathlib.Path.cwd()
# output Windows: WindowsPath(r"C:\Users\David\Documents")
# output Linux: PosixPath('/home/mint/Documents/z2j_path_python')

# 3. With the / operator
# create a Path object representing a file namend hello.txt in the Documents subdirectory of the current user's home directory
home / "Desktop" / "hello.txt"
# output Linux: PosixPath('/home/mint/Desktop/hello.txt')

# Absolute vs. Relative Paths
"""
A path tha begins with the root directory in a file system is called an 'absolute file path'. 
A file path that is not absolute is called a 'relative file path'.
"""
# Relative Windows path
path = pathlib.Path(r"Photos\image.jpg")
# Relative macOS or Linux path
path = pathlib.Path("Photos/image.jpg")

path.is_absolute()  # False - check a file path is absolute or not

# extend relative path to an absolute:
home = pathlib.Path.home()
home / pathlib.Path("Photos/image.jpg")  # PosixPath('/home/mint/Photos/image.jpg')

# Accessing File Path Components
path = pathlib.Path.home() / "hello.txt"
path  
# outout: PosixPath('/home/mint/hello.txt')
list(path.parents)
# output: [PosixPath('/home/mint'), PosixPath('/home'), PosixPath('/')]

# You can iterate over the parent directories in a for loop:
for directory in path.parents:
    print(directory)
""" 
output:
/home/mint
/home
/
"""
# The .parent attribute returns the name of the first parent directory in the file path as a string:
path.parent
# output: PosixPath('/home/mint')

# If the file path is absolute, you can access to the root directory of the file path with the .anchor attribute (it returns a string) - for relative paths it returns an empty string: 
path.anchor
# output: '/'

# The .name attribute returns the name of the file or directory that the path points to:
home = pathlib.Path.home()  # PosixPath('/home/mint')
home.name  # 'mint'
path = home / "hello.txt"  # PosixPath('/home/mint/hello.txt')
path.name  # 'hello.txt'

"""
The name of a file is broken down into two parts.
The part to the left of the dot (.) is called the stem,
and the part to the right of the dot (.) is called suffix or file extension.
"""
# The .stem and .suffix attributes return strings containing each of these parts of the file name:
path = pathlib.Path.home() / "hello.txt"
path.stem  # 'hello'
path.suffix  # '.txt'

# Checking Whether Or Not a File Path Exists
# To check if the file exist:
path = pathlib.Path.home() / "hello.txt"
path.exists()  # False

# To check if the path is a file, use the .is_file() method (if the file path refers to a file, but file doesn't exist, then returns False):
path.is_file()  # True

# To chesk if the file path refers to a directory, use the .is_dir():
path.is_dir()  # False (the path to "hello.txt is not a directory")
home.is_dir()  # True (the path to home directory is a directory)

# *************************************************************************************

# Common File System Operations

# Creating Directories and Files
from pathlib import Path
new_dir = Path.home() / "new_directory"  # create new path to a directory in home folder, and assign to the new_dir variable
new_dir.mkdir()  # create new directory

new_dir.exists()  # check new directory exists
True
new_dir.is_dir()  # check new directory is a directory
True

# If you try to create a directory thar already exists, you get an error: FileExistsError

# When you execute .mkdir() with the exist_ok parameter set to True, the directory is created if it does not exist, or nothing happens if it does.
new_dir.mkdir(exist_ok=True)
# it is equivalent to the following code:
if not new_dir.exists():
    new_dir.mkdir()

# If you try to create a subdirectory within a directory that does not exist:
nested_dir = new_dir / "folder_a" / "folder_b"
nested_dir.mkdir()
# You get an error: FileNotFoundError
# To create any parent directories needed in order to create the target directory, set the optional parents parameter of .mkdir() to True:
nested_dir.mkdir(parents=True)

""" 
Common pattern for creating directories.
The entire path is created, if needed, and no exception is raised if thr path already exist. 
"""
path.mkdir(parents=True, exist_ok=True)

# Create files
file_path = new_dir / "file1.txt"  # create new Path object
file_path.touch()  # create file

file_path.exists()  # check new file exists
True
file_path.is_file()  # check new file is a file
True

# You can't create a file in a directory that doesn't exist: File NotFoundError
file_path = new_dir / "folder_c" / "file2.txt"
file_path.touch()

# You can use .parent to get the path to the parent folder for file2.txt and then call .mkdir() to create the directory:
file_path.parent.mkdir()

# *************************************************************************************

# Iterating Over Directory Contents
# Path.iterdir() method returns an iterator over Path objects representing each item inthe directory:
for path in new_dir.iterdir():
    print(path)
# /home/mint/new_directory/file1.txt
# /home/mint/new_directory/folder_a
# /home/mint/new_directory/folder_c

# You can convert it to a list:
list(new_dir.iterdir())
# [PosixPath('/home/mint/new_directory/file1.txt'), PosixPath('/home/mint/new_directory/folder_a'), PosixPath('/home/mint/new_directory/folder_c')]

# *************************************************************************************

# Searching For Files In a Directory
# use the Path.glob() method on a path representing a directory to get an iterable over directory contents that meet some criteria.
# A wildcard character is a special character that acts as a placeholder in a pattern.
for path in new_dir.glob("*.txt"):
    print(path)
# /home/mint/new_directory/file1.txt

# You ca convert it to a list:
list(new_dir.glob("*.txt"))
# [PosixPath('/home/mint/new_directory/file1.txt')]

"""
Some wildcard characters:
* - any number of characters (example: "*b*") (matches: b, ab, bc, abc) (not matches: a, c, ac)
? - a single character (examle: "?bc") (matches: abc, bbc, cbc) (not matches: bc, aabc, abcd)
[abc] - one character in the brackets (example: [CB]at) (matches: Cat, Bat) (not matches: at, cat, bat)
"""

# create more files:
paths = [
    new_dir / "program1.py",
    new_dir / "program2.py",
    new_dir / "folder_a" / "program3.py",
    new_dir / "folder_a" / "folder_b" / "image1.jpg",
    new_dir / "folder_a" / "folder_b" / "image2.jpg",
]
for path in paths:
    path.touch()

## The * Wildcard
list(new_dir.glob("*.py"))
# [PosixPath('/home/mint/new_directory/program2.py'), PosixPath('/home/mint/new_directory/program1.py')]

list(new_dir.glob("*1*"))
# [PosixPath('/home/mint/new_directory/file1.txt'), PosixPath('/home/mint/new_directory/program1.py')]

list(new_dir.glob("1*"))
# []

## The ? Wildcard
list(new_dir.glob("program?.py"))
# [PosixPath('/home/mint/new_directory/program2.py'), PosixPath('/home/mint/new_directory/program1.py')]

list(new_dir.glob("?older_?"))
# [PosixPath('/home/mint/new_directory/folder_a'), PosixPath('/home/mint/new_directory/folder_c')]

list(new_dir.glob("*1.??"))
# [PosixPath('/home/mint/new_directory/program1.py')]

## The [] Wildcard
list(new_dir.glob("program[13].py"))
# [PosixPath('/home/mint/new_directory/program1.py')]

# *************************************************************************************

# Recursive Matching Wirh The ** Wildcard

# Prefix your pattern with "**/", tells .glob() to match your pattern in the current directory and any of its subdirectories.
list(new_dir.glob("**/*.txt"))
# [PosixPath('/home/mint/new_directory/file1.txt'), 
# PosixPath('/home/mint/new_directory/folder_c/file2.txt')]

list(new_dir.glob("**/*.py"))
# [PosixPath('/home/mint/new_directory/program2.py'), 
# PosixPath('/home/mint/new_directory/program1.py'), 
# PosixPath('/home/mint/new_directory/folder_a/program3.py')]

# Shorthand method do diong recursive:
list(new_dir.rglob("*.py"))

# *************************************************************************************

# Moving and Deleting Files and Folders

# To move a file or directory, use the .replace() method
source = new_dir / "file1.txt"
destination = new_dir / "folder_a" / "file1.txt"
source.replace(destination)
# PosixPath('/home/mint/new_directory/folder_a/file1.txt')

"""
If the destination path already exists, .replace() overwrites the destination with the source file without raising any kind of exeption. This can cause undesired loss of data if you aren't careful.
You may want to first check if the destination file exists, and move the file only in the case that it does not:
"""
if not destination.exists():
    source.replace(destination)
    
# To move or rename an antire directory:
source = new_dir / "folder_c"
destination = new_dir / "folder_d"
source.replace(destination)
# PosixPath('/home/mint/new_directory/folder_d')

# To delete a file, use the .unlink() method:
file_path = new_dir / "program1.py"
file_path.unlink()

# check the file exists:
file_path.exists()  # False

# You can also see it removed with .iterdir():
list(new_dir.iterdir())
# [PosixPath('/home/mint/new_directory/program2.py'), 
# PosixPath('/home/mint/new_directory/folder_a'), 
# PosixPath('/home/mint/new_directory/folder_d')]

# If the path that you call .nlink() does not exists, a FileNotFoundError exception is raised.
# If you want to ignore the exception, set the optional missing_ok parameter to True:
file_path.unlink(missing_ok=True)

# To remove directory, us the .rmdir() method. Folder must be empty, otherwise an OSError is raised.
# First delete all of the files it contains:
folder_d = new_dir / "folder_d"

for path in folder_d.iterdir():
    path.unlink()

folder_d.rmdir()

folder_d.exists()  # False

"""
To delete an entire directory, even if it is non-empty, you can use the rmtree() function from the built-in shutil module:
"""
import shutil
folder_a = new_dir / "folder_a"
shutil.rmtree(folder_a)

folder_a.exists()  # False
list(new_dir.rglob("image*.*"))  # []

# *************************************************************************************
# Reading and Writing Files

# Character Encoding
# UTF-8

# Line Endings
# escape sequence:
# \r carriage return
# \n line feed

# text in Windows
"""
Jack Russell Terrier\r\n
English Springer Spaniel\r\n
German Shepherd\r\n
"""
# text on macOS or Ubuntu
"""
Jack Russell Terrier\r
\n
English Springer Spaniel\r
\n
German Shepherd\r
\n
"""

# *************************************************************************************

# Python File Objects

# The Path.open() Method
from pathlib import Path
path = Path.home() / "hello.txt"  # create Path object for hello.txt and assign to path
path.touch()  # create file in home directory
file = path.open(mode="r", encoding="utf-8")  # create file object and asign to file variable

file  # <_io.TextIOWrapper name='/home/mint/hello.txt' mode='r' encoding='utf-8'>

"""
Mode
"r" - Creates a text ﬁle object for reading and raises an error if the ﬁle can’t be opened.
"w" - Creates a text ﬁle object for writing and overwrites all existing data in the ﬁle.
"a" - Creates a text ﬁle object for appending data to the end of a ﬁle.
"rb" - Creates a binary ﬁle object for reading and raises an error if the ﬁle can’t be opened.
"wb" - Creates a binary ﬁle object for writing and overwrites all existing data in the ﬁle.
"ab" - Creates a binary ﬁle object for appending data to the end of the ﬁle
"""

"""
String - Character Encoding
"ascii" - ASCII
"utf-8" - UTF-8
"utf-16" - UTF-16
"utf-32" - UTF-32
"""

# close a file
file.close()

# The open() Build-in
file_path = "/home/mint/hello.txt"  # create nariable containing the path to the hello.txt
file = open(file_path, mode="r", encoding="utf-8")  #create file object and assign to variable
file.close()

# The with Statement
with path.open(mode="r", encoding="utf-8") as file:
    # Do something with file
    
# with statements also work with the open() built-in:
with open(file_path, mode="r", encoding="utf-8") as file:
    # Do something with file

# Reading Data From File

path = Path.home() / "hello.txt"
with path.open(mode="r", encoding="utf-8") as file:
    text = file.read()  # reads all of the text in the file and returns it as a string 

type(text)  # <class 'str'>

text  # 'Hello World'

# add second line of the text to the file
with path.open(mode="r", encoding="utf-8") as file:
    text = file.read()
    
text  # 'Hello World\nHello again'

# reading each line of the file one at a time:
with path.open(mode="r", encoding="utf-8") as file:
    for line in file.readlines():
        print(line)
"""
Hello World

Hello again
"""

# lines without the extra blank lines
with path.open(mode="r", encoding="utf-8") as file:
    for line in file.readlines():
        print(line, end="")  # added empty end parapetr
"""
Hello World
Hello again
"""

# if file does not exists, raise a FileNotFoundError

# Writing Data To a File
with path.open(mode="w", encoding="utf-8") as file:
    file.write("Hi there!")
# output: 9
# write() returns the number of characters that are written
"""
When you set mode="w" in open(), the contents of the original file are overwritten.
"""

# append mode (append data to the end of a file)
with path.open(mode="a", encoding="utf-8") as file:
    file.write("\nHello")
# output: 6

# Write multiple lines to a file at the same time using .writelines() method
lines_of_text = [
    "Hello from Line 1\n",
    "Hello from Line 2\n",
    "Hello from line 3 \n",
]
with path.open(mode="w", encoding="utf-8") as file:
    file.writelines(lines_of_text)

with path.open(mode="r", encoding="utf-8") as file:
    # for line in file.readlines():
    #     print(line, end="")
    text = file.read()

print(text)
# Hello from Line 1
# Hello from Line 2
# Hello from line 3 

# If you want to write to a path with parent folders that may not exist:
path = Path.home() / "new_folder" / "new_file.txt"
path.parent.mkdir(parents=True)
with path.open(mode="w", encoding="utf-8") as file:
    file.write("Hello!")
# output: 6

# *************************************************************************************

# Read and Write CSV Data

temperature_readings = [68, 65, 68, 70, 74, 72]
# To store these values to a file, you can write the values from each day on a new line in a text file and separate each value with a comma.
from pathlib import Path
file_path = Path.home() / "temperatures.csv"
with file_path.open(mode="a", encoding="utf-8") as file:
    file.write(str(temperature_readings[0]))
    for temp in temperature_readings[1:]:
            file.write(f",{temp}")

"""
2
3
3
3
3
3
"""
# Reading the text
with file_path.open(mode="r", encoding="utf-8") as file:
    text = file.read()

text
# output:'68,65,68,70,74,72'
# CSV - Comma Separated Value

# Convert CSV records as a list:
temperatures = text.split(",")
temperatures
# output: ['68', '65', '68', '70', '74', '72']

# The values in the temperatures list are strings (origilanlly written to the CSV file are integers). This is because values read from a text file are always read as strings.

# Convert strings to integers using a list comprehension:
int_temperatures = [int(temp) for temp in temperatures]
int_temperatures
# output: [68, 65, 68, 70, 74, 72]

# The csv Module

import csv

# Writing csv Files With csv.writer

daily_temperatures = [
    [68, 65, 68, 70, 74, 72],
    [67, 67, 70, 72, 72, 70],
    [68, 70, 74, 76, 74, 73],
]

file_path = Path.home() / "temperatures.csv"
file = file_path.open(mode="w", encoding="utf-8")
# Instead of using a with statement, a file object is created and assigned to the file variable so that we can inspect each step of the writing process as we go.

writer = csv.writer(file)  # create a new csv writer object

# You can use the writer.writerow() method to write a list to a new row in the csv file:
for temp_list in daily_temperatures:
    writer.writerow(temp_list)

"""
19
19
19
"""
# .writerow() returns the number of characters written to the ﬁle.

file.close()

# The same code using the with statement:
with file_path.open(mode="w", encoding="utf-8") as file:
    writer = csv.writer(file)
    for temp_list in daily_temperatures:
        writer.writerow(temp_list)

# You can write multiple rows at one using the .writerows() method.
with file_path.open(mode="w", encoding="utf-8") as file:
    writer = csv.writer(file)
    writer.writerows(daily_temperatures)

# Reading csv Files With csv.reader

file = file_path.open(mode="r", encoding="utf-8")
reader = csv.reader(file)

for row in reader:
    print(row)
"""
['68', '65', '68', '70', '74', '72']
['67', '67', '70', '72', '72', '70']
['68', '70', '74', '76', '74', '73']
"""

file.close()

# example using that open the CSV ﬁle in a with statement, reads each row in the CSV ﬁle, converts the list of strings to a list of integers, and stores each list of integers in a list of lists called daily_temperatures:

# Create an empty list
daily_temperatures = []
with file_path.open(mode="r", encoding="utf-8") as file:
    reader = csv.reader(file)
    for row in reader:
        # Convert row to list of integers
        int_row = [int(value) for value in row]
        # Append the list of integers to daily_temperatures list
        daily_temperatures.append(int_row)

daily_temperatures
# output: [[68, 65, 68, 70, 74, 72], [67, 67, 70, 72, 72, 70], [68, 70, 74, 76, 74, 73]]

# Reading and Writing CSV Files With Headers

# create new csv.DictReader object:
from pathlib import Path
import csv
file_path = Path.home() / "employees.csv"
file = file_path.open(mode="r", encoding="utf-8")
reader = csv.DictReader(file)

# the first row of the csv file is assumed to contain the field names:
reader.fieldnames
# output: ['name', 'department', 'salary']

# DictReader object are iterable
for row in reader:
    print(row)

"""
{'name': 'Lee', 'department': 'Operations', 'salary': '75000.00'}
{'name': 'Jane', 'department': 'Engineering', 'salary': '85000.00'}
{'name': 'Diego', 'department': 'Sales', 'salary': '80000.00'}
"""

file.close()

# DictReader objects return each row as a dictionary.

# Function that converts keys to the correct data types:
def process_row(row):
    row["salary"] = float(row["salary"])
    return row

with file_path.open(mode="r", encoding="utf-8") as file:
    reader = csv.DictReader(file)
    for row in reader:
            print(process_row(row))
"""
{'name': 'Lee', 'department': 'Operations', 'salary': 75000.0}
{'name': 'Jane', 'department': 'Engineering', 'salary': 85000.0}
{'name': 'Diego', 'department': 'Sales', 'salary': 80000.0}
"""

# csv.DictWriter
people = [
    {"name": "Veronica", "age": 29},
    {"name": "Audrey" , "age": 32},
    {"name": "Sam", "age": 24},
]

file_path = Path.home() / "people.csv"
file = file_path.open(mode="w", encoding="utf-8")
writer = csv.DictWriter(file, fieldnames=["name", "age"])
# You could also set the fieldnames parameter to people[0].keys()

writer.writeheader()  # writes the header row to the csv file
# output: 10

writer.writerows(people)  # write the data in the people list to the csv file
file.close()



# str. 387