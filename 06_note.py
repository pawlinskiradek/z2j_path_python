# p-101-l04

# Functions and Loops
del len # del i used to un-assign a variable from a value

# Functions with no return statement
def greet(name):
    print(f"Hello, {name}!")
greet("Radek") # 'Hello, Radek!'

# Documenting Your Functions
def multiply(x, y): # Function signature
    """Return the product of two numbers x and y.""" # docstring
    # Function body
    product = x * y
    return product
>>> help(multiply)
# Help on function multiply in module __main__:
# 
# multiply(x, y)
    # Return the product of two numbers x and y.

# The while Loop - repeat some code while some condition remains true
num = float(input("Enter a positive number: "))

while num <= 0:
    print("That's not a positive number!")
    num = float(input("Enter a positive number: "))

# why for loops are better for looping over collections of items:
word = "Python"
index = 0

while index < len(word):
    print(word[index])
    index = index + 1

# The for Loop - repeat some code for each element in a set of objects
for letter in "Python":
    print(letter)

# range(3) returns the range of integers starting with 0 and up to, but not including, 3.

for n in range(3):
    print("Ptyhon")
# prints the string "Python" three times.

# range(1, 5) give range starting point, end point is not included
for n in range(10, 20):
    print(n * n)

# Nested Loops
for n in range(1, 4):
    for j in range(4, 7):
        print(f"n = {n} and j = {j}")

# Scope in Python
x = "Hell World"  # global scope

def func():
    x = 2  # local scope
    print(f"Inside 'func', x has the value {x}")

func()
print(f"Outside 'func', x has a value {x}")
# output: Inside 'func', x has the value 2
# Outside 'func', x has the value Hello World

# Scope Resolution
x = 5
def outer_func():
    y = 3
    
    def inner_func():  # inner function
        z = x + y
        return z
    
    return inner_func()

# The LEGB Rule (Local, Enclosing, Global, Built-in)


# strona 167