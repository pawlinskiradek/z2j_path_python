"""
we can write a function that simulates a coin flip, but
returns tails with a speciﬁed probability
"""

import random

def unfair_coin_flip(probability_of_tails):
    if random.random() < probability_of_tails:
        return "tails"
    else:
        return "heads"
# for example, unfair_coin_flip(.7) has a 70% chance of returning "tails"

heads_tally = 0
tails_tally = 0

for trial in range(10_000):
    if unfair_coin_flip(.7) == "heads":
        heads_tally = heads_tally + 1
    else:
        tails_tally = tails_tally + 1

ratio = heads_tally / tails_tally
print(f"The ratio of heads to tails is {ratio}")