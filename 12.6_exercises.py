# 1. Write a script that writes the following list of lists to a ﬁle called numbers.csv in your home directory:
"""
numbers = [
[1, 2, 3, 4, 5],
[6, 7, 8, 9, 10],
[11, 12, 13, 14, 15],
]
"""
import csv
from pathlib import Path

numbers = [
[1, 2, 3, 4, 5],
[6, 7, 8, 9, 10],
[11, 12, 13, 14, 15],
]

file_path = Path.home() / "numbers.csv"

# write multiple rows at one:
with file_path.open(mode="w", encoding="utf-8") as file:
    writer = csv.writer(file)
    writer.writerows(numbers)

# 2. Write a script that reads the numbers in the numbers.csv ﬁle from Exercise 1 into a list of lists of integers called numbers. Print the list of lists. Your output should like the following:
"""
[[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13, 14, 15]]
"""

# reading csv file:
numbers = []
with file_path.open(mode="r", encoding="utf-8") as file:
    reader = csv.reader(file)
    for row in reader:
        # convert rows to list of integers
        int_row = [int(value) for value in row]
        # append list of integers to the numbers list
        numbers.append(int_row)

print(numbers)

# 3. Write a script that writes the following list of dictionaries to a ﬁle called favorite_colors.csv in your home directory:
"""
favorite_colors = [
{"name": "Joe", "favorite_color": "blue"},
{"name": "Anne", "favorite_color": "green"},
{"name": "Bailey", "favorite_color": "red"},
]
"""
# The output CSV ﬁle should have the following format:
"""
name,favorite color
Joe,blue
Anne,green
Bailey,red
"""

favorite_colors = [
{"name": "Joe", "favorite_color": "blue"},
{"name": "Anne", "favorite_color": "green"},
{"name": "Bailey", "favorite_color": "red"},
]

file_path = Path.home() / "favorite_colors.csv"

with file_path.open(mode="w", encoding="utf-8") as file:
    writer = csv.DictWriter(file, fieldnames=favorite_colors[0].keys())
    writer.writeheader()  # writes headers to the csv file
    writer.writerows(favorite_colors)  # write data in the favorice_color list to the csv file

# 4. Write a script that reads the data from the favorite_colors.csv ﬁle from Exercise 3 into a list of dictionaries called favorite_colors.
# Print the list of dictionaries. The output should look something like this:
"""
[{"name": "Joe", "favorite_color": "blue"},
{"name": "Anne", "favorite_color": "green"},
{"name": "Bailey", "favorite_color": "red"}]
"""

favorite_colors = []

with file_path.open(mode="r", encoding="utf-8") as file:
    reader = csv.DictReader(file)
    for row in reader:
        favorite_colors.append(row)

print(favorite_colors)