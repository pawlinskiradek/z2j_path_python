"""Convert Temperatures
Write a script called temperature.py that deﬁnes two functions:
1. convert_cel_to_far() which takes one float parameter representing
degrees Celsius and returns a float representing the same temper-
ature in degrees Fahrenheit using the following formula:
F = C * 9/5 + 32
2. convert_far_to_cel() which take one float parameter representing
degrees Fahrenheit and returns a float representing the same tem-
perature in degrees Celsius using the following formula:
C = (F - 32) * 5/9
"""
    
def convert_cel_to_far(cel):
    return cel * 9/5 + 32

def convert_far_to_cel(far):
    return (far - 32) * 5/9

far = float(input('Enter a temperature in degrees F: '))
print(f'{far} degrees F = {convert_far_to_cel(far):.2f} degrees C')

cel = float(input('Enter a temperature in degrees C: '))
print(f'{cel} degrees C = {convert_cel_to_far(cel):.2f} degrees F')