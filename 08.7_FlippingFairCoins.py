"""
Flipping Fair Coins
One trial for our experiment will be flipping the coin. The outcome is
either a heads or a tails. The question is: in general, over many coin
flips, what is the ratio of heads to tails?
"""

import random

def coin_flip():
    """Randomly return 'heads' or 'tails'. """
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"
    
# First initialize the tallies to 0
heads_tally = 0
tails_tally = 0

for trial in range(10_000):
    if coin_flip() == "heads":
        heads_tally = heads_tally + 1
    else:
        tails_tally = tails_tally + 1

ratio = heads_tally / tails_tally
print(f"The ratio of heads to tails is {ratio}")