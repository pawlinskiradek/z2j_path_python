# 1. Write a function called cube() with one number parameter and returns the value of that number raised to the third power. Test the function by displaying the result of calling your cube() function on a few diﬀerent numbers.
def cube(num):
    """Returns the value of num raised to the third power."""
    return num ** 3

print(cube(2))
print(cube(-3))
print(cube(1))
# 2. Write a function called greet() that takes one string parameter called name and displays the text "Hello <name>!", where <name> is replaced with the value of the name parameter.
def greet(name):
    """displays the text "Hello <name>!", where <name> is the value from name parametr"""
    print(f"Hello {name}!")
    
greet("Radek")
greet("Ania")
greet("Seba")