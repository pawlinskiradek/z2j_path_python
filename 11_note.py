# Modules and Packages

# Creating Modules
# adder.py

def add(x, y):
    return x + y

def double(x):
    return x + x

# main.py

import adder  # Importing One Module Into Another

value = adder.add(2, 2)
double_value = adder.double(value)
print(value)
print(double_value)

# Import Statement Variations
# 1. import <module> as <other_name>
import adder as a

value = a.add(2, 2)
double_value = a.double(value)
# 2. from <module> import <name>
from adder import add

value = add(2, 2)  # you can use add() without having to type adder.add()
double_value = double(value)  # NameError: name 'adder' is not defined

# Creating Packages
# A package is a folder tha contains one or more Python modules. It must also contain a special module called __init__.py (doesn't need to contain any code inside!)
"""
mypackage/
    - __init__.py
    - module1.py
    - module2.py
"""
# Importing Modules from Packages
# dotted module names
# import <package_name>.<module_name>

# Import Statement Variations For Packages
"""
1. import <package>
2. import <package> as <other_name>
3. from <package> import <module>
4. from <package> import <module> as <other_name>
"""

# Importing Modules From Subpackages
"""
mypackage/
    - mysubpackage/
        - __init__.py
        - module3.py
    
    - __init__.py
    - module2.py
    - module2.py
"""



# str. 326