# Deﬁne a function, enrollment_stats(), that takes, as an input, a list of
# lists where each individual list contains three elements: (a) the name
# of a university, (b) the total number of enrolled students, and (c) the
# annual tuition fees.

# enrollment_stats() should return two lists: the ﬁrst containing all of
# the student enrollment values and the second containing all of the
# tuition fees.
def enrollment_stats(l1):
    lb = []
    lc = []
    for element in l1:
        lb.append(element[1])
        lc.append(element[2])
    return(lb, lc)

# Next, deﬁne a mean() and a median() function. Both functions should
# take a single list as an argument and return the mean and median of
# the values in each list.
def mean(values):
    return sum(values) / len(values)

def median(values):
    values.sort()
    if len(values) %2 == 1:
        center_index = int(len(values) / 2)
        return values[center_index]
    else:
        left_index = (len(values) - 1) // 2  # integer division
        right_index = (len(values) + 1) // 2
        return mean([values[left_index], values[right_index]])
        # return sum([values[left_index], values[right_index]]) / 2
        
universities = [
['California Institute of Technology', 2175, 37704],
['Harvard', 19627, 39849],
['Massachusetts Institute of Technology', 10566, 40732],
['Princeton', 7802, 37000],
['Rice', 5879, 35551],
['Stanford', 19535, 40569],
['Yale', 11701, 40500]
]

all_students = enrollment_stats(universities)[0]
all_tuition_fees = enrollment_stats(universities)[1]

print("*****" * 6)
print(f"Total students:     {sum(all_students):,}")
print(f"Total tuition:    $ {sum(all_tuition_fees):,}")
print()
print(f"Student mean:       {mean(all_students):,.2f}")
print(f"Student median:     {median(all_students):,}")
print()
print(f"Tuition mean:     $ {mean(all_tuition_fees):,.2f}")
print(f"Tuition median:   S {median(all_tuition_fees):,}")
print("*****" * 6)