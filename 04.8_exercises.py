# 1. In one line of code, display the result of trying to .find() the substring "a" in the string "AAA". The result should be -1.
print("AAA".find("a"))

# 2. Replace every occurrence of the character "s" with "x" in the string "Somebody said something to Samantha.".
print("Somebody said something to Samantha.".replace("s", "x"))

# 3. Write and test a script that accepts user input using the input() function and displays the result of trying to .find() a particular letter in that input.
word = input("Podaj jakiś wyraz: ")
letter = input("jaką literę znaleźć?: ")
print(f"W wyrazie {word} litera {letter} występuje po raz pierwszy pod indeksem {word.find(letter)}.")