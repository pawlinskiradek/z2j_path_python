# Challenge: Create a High Scores List

# In the Chapter 12 Practice Files folder, there is a CSV ﬁle called scores.csv containing data about game players and their scores. The ﬁrst few lines of the ﬁle look like this:
"""
name, score
LLCoolDave,23
LLCoolDave,27
red,12
LLCoolDave,26
tom123,26
"""
# Write a script that reads the data from this CSV ﬁle and creates a new ﬁle called high_scores.csv where each row contains the player name and their highest score. The output CSV ﬁle should look like this:
"""
name,high_score
LLCoolDave,27
red,12
tom123,26
O_O,7
Misha46,25
Empiro,23
MaxxT,25
L33tH4x,42
johnsmith,30
"""
import csv
from pathlib import Path

# Function to process a row, converting the score to an integer
def process_row(row):
    row["score"] = int(row["score"])
    return row

# Path to the input CSV file
file_path = Path.home() / "scores.csv"

# Dictionary to store the highest score for each player
high_scores = {}

# Read and process rows from the input CSV file
with file_path.open(mode="r", encoding="utf-8") as file:
    reader = csv.DictReader(file)
    for row in reader:
        # Process the current row
        processed_row = process_row(row)
        name = processed_row["name"]
        score = processed_row["score"]

        # Update the highest score for the player
        if name in high_scores:
            if score > high_scores[name]:
                high_scores[name] = score
        else:
            high_scores[name] = score

# Path to the output CSV file
new_file_path = Path.home() / "high_scores.csv"

# Write the highest scores to the new CSV file
with new_file_path.open(mode="w", encoding="utf-8") as file:
    writer = csv.writer(file)

    # Write the header row
    writer.writerow(["name", "high_score"])

    # Iterate through the high_scores dictionary and write each player's name and highest score
    for name, score in high_scores.items():
        writer.writerow([name, score])

# Optionally print the high_scores dictionary for reference
print(high_scores)
