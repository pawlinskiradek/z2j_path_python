"""
Track Your Investments
In this challenge, you will write a program called invest.py that tracks
the growing amount of an investment over time.
An initial deposit, called the principal amount, is made. Each year,
the amount increases by a ﬁxed percentage, called the annual rate of
return.
"""
def invest(amount, rate, years):  # the principal amount, the annual rate of return, the number of years to calculate
    for year in range(1, years + 1):
        amount = amount + amount * rate  # amount * (1 + rate)
        print(f"year {year}: ${amount:.2f}")
        
amount = float(input("Enter an initial amount: "))
rate = float(input("Enter an annual percentage rate (100% = 1, 50% = 0.5 ...): "))
years = int(input("Enter a number of years to calculate: "))

invest(amount, rate, years)

