# Tuples, Lists, and Dictionaries

# Tuple Literals
my_first_tuple = (1, 2, 3)
empty_tuple = ()

x = (1)  # <class 'int'>
x = (1,)  # <class 'tuple'>

# The tuple() Built-In

tuple("Python")  # ('P', 'y', 't', 'h', 'o', 'n')

tuple(1, 2, 3)  # TypeError
tuple(1)  # TypeError

tuple()  # ()

# Lenght
numbers = (1, 2, 3)
len(numbers)  # 3

# Indexing
name = tuple("David")
name[1]  # 'a'

# Slicing
name = tuple("David")
name[2:4]  # "vi"

# Tuples Are Immutable
# Like strings, tuples are immutable. This means you can’t change the
# value of an element of a tuple once it has been created.

# Tuple Packing and Unpacking
coordinates = 4.21, 9.29  # <class 'tuple'> packed
x, y = coordinates  # x=4.21 y=9.29 unpacked

name, age, ocupation = "David", 34, "programmer"

# Checking Existence of Values With in
vowels = ("a", "e", "i", "o", "u")
"o" in vowels  # True

# Returning Multiple Values From a Function
def adder_subtractor(num1, num2):
    return(num1 + num2, num1 - num2)

adder_subtractor(3, 2)  # (5, 1)

# **************************************************************************
# Lists Are Mutable Sequences
# you can change the value at an index even after the list has been created.

# List literal
colors = ["red", "yellow", "green", "blue"]  # <class 'list'>

# list() Built-in
list((1, 2, 3))  # tuple can be passed to list [1, 2, 3]
list("Python")  # list fro string ['P', 'y', 't', 'h', 'o', 'n']

# Create a list from a string of a coma-separated list of items using thw string object'a .split() method:
groceries = "eggs, milk, cheese"
grocery_list = groceries.split(", ")
print(grocery_list)  # ['eggs', 'milk', 'cheese']

"abbaabba".split("ba")  # ['ab', 'ab', ''] 
# The string "abbaabba"
# contains two instances of the separator "ba" so the list returned by
# split() has three elements. Since the third separator isn’t followed by
# any other characters, the third element of the list is set to the empty
# string.

"abbaabba".split("c")  # ['abbaabba']
# If the separator is not contained in the string at all, .split() returns a
# list with the string as its only element

# Basic List Operation

# Index Notation:
numbers = [1, 2, 3, 4]
numbers[1]  # 2

# create new list using slice:
numbers[1:3]  # [2, 3]

# check existence using in operato:
"Bob" in numbers  # False

# iterate over list with a for loop:
for number in numbers:
    if number % 2 == 0:  # only the even numbers
        print(number)  # 2, 4

# The major difference between lists and tuples is that elements of lists 
# may be changed, but elements of tuples can not.

# Changing Elements in a List
# The ability to swap values in a list for other values is called MUTABILITY.
# Lists are MUTABLE, and tuples are IMMUTABLE.

# Swap values using index notation:
colors = ["red", "yellow", "green", "blue"]
colors[0] = "burgundy"
colors  # ['burgundy', 'yellow', 'green', 'blue']

# Change several values using slice assigment:
colors[1:3] = ["orange", "magenta"]
colors  # ['burgundy', 'orange', 'magenta', 'blue']

# Assign a list of three elements to slice with two elements:
colors = ["red", "yellow", "green", "blue"]
colors[1:3] = ["orange", "magenta", "aqua"]
colors  # ['red', 'orange', 'magenta', 'aqua', 'blue']

# Asign a list of two elements to slice with three elements:
colors  # ['red', 'orange', 'magenta', 'aqua', 'blue']
colors[1:4] = ["yellow", "green"]
colors  # ['red', 'yellow', 'green', 'blue']

# List Methods For Adding and Removing Elements
# *********************************************************************
list.insert()
colors = ["red", "yellow", "green", "blue"]
colors.insert(1, "orange")  # insetr "orange" into the second position
colors  # ['red', 'orange', 'yellow', 'green', 'blue']

colors.insert(20, "violet")
colors  # ['red', 'orange', 'yellow', 'green', 'blue', 'violet']

colors.insert(-1, "indigo")
colors  # ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet']

# *********************************************************************
list.pop()  # remove value from the list at specific index
color = colors.pop(3)
color  # 'green'
colors  # ['red', 'orange', 'yellow', 'blue', 'indigo', 'violet']

colors.pop(-1)  # 'violet'
colors  # ['red', 'orange', 'yellow', 'blue', 'indigo']

colors.pop()  # 'indigo'
colors  # ['red', 'orange', 'yellow', 'blue']

# *********************************************************************
list.append()  # append element to the end of the list
colors.append("indigo")  # colors.insert(len(colors), "indigo")
colors  # ['red', 'orange', 'yellow', 'blue', 'indigo']

# *********************************************************************
list.extend()  # add several new elements to the end of a list
colors.extend(["violet", "ultraviolet"])
colors  # ['red', 'orange', 'yellow', 'blue', 'indigo', 'violet', 'ultraviolet']
colors.extend(("violet", "ultraviolet"))

# *********************************************************************
# List of Numbers (the built-in functions)
# Add up all the values to get the total:
# with a loop:
nums = [1, 2, 3, 4, 5]
total = 0
for number in nums:
    total = total + number
total  # 15
# with built-in functions sum(), min(), max() // works with tuples also
sum([1, 2, 3, 4, 5])  # 15
min([1, 2, 3, 4, 5])  # 1
max([1, 2, 3, 4, 5])  # 5

# *********************************************************************
# List Comprehensions
numbers(1, 2, 3, 4, 5)
squares = [num**2 for num in numbers]
squares  # [1, 4, 9, 16, 25]
# same result:
squares = []
for num in numbers:
    squares.append(num**2)
squares  # [1, 4, 9, 16, 25]
# List comprehension are commonly used to convert values in on list to a different type.
str_numbers = ["1.5", "2.3", "5.25"]
float_numbers = [float(value) for value in str_numbers]
float_numbers  # [1.5, 2.3, 5.25]

# *********************************************************************
# Nesting, Coping and Sorting Tuples ans Lists

# Nesting Lists and Tuples
two_by_two = [[1, 2], [3, 4]]
len(two_by_two)  # 2
two_by_two[0]  # first element two_by_two [1, 2]
two_by_two[1]  # second element two_by_two [3, 4]

# Double index notation
two_by_two[1][0]  # 3

# Copying a List
animals = ["lion", "tiger", "frumious Bandersnatch"]
large_cats = animals  # variables refer to the same object
large_cats.append("Tigger")
animals  # ['lion', 'tiger', 'frumious Bandersnatch', 'Tigger']

animals = ["lion", "tiger", "frumious Bandersnatch"]
large_cats = animals[:]
large_cats.append("leopard")
large_cats  # ['lion', 'tiger', 'frumious Bandersnatch', 'leopard']
animals  # ["lion", "tiger", "frumious Bandersnatch"]

# Shallow Copy is only one level deep. The copying process does not recurse and therefore won’t create copies of the child objects themselves.
matrix1 = [[1, 2], [3, 4]]
matrix2 = matrix1[:]
matrix2[0] = [5, 6]
matrix2  # [[5, 6], [3, 4]]
matrix1  # [[1, 2], [3, 4]]

matrix2[1][0] = 1
matrix2  # [[5, 6], [1, 4]] - child element reference to the same object in memory
matrix1  # [[1, 2], [1, 4]]
# Deep Copy. Copying an object this way walks the whole object tree to create a fully independent clone of the original object and all of its children.

# Sorting Lists
# strings are sorted alphabetically
colors = ["red", "yellow", "green", "blue"]
colors.sort()
colors  # ['blue', 'green', 'red', 'yellow']
# numbers are sorted numerically
numbers = [1, 10, 5, 3]
numbers.sort()
numbers  # [1, 3, 5, 10]

# The key parapeter accepts a function (without any parentheses), and the list is sorted based on the return value of that function.
colors = ["red", "yellow", "green", "blue"]
colors.sort(key=len)
colors  # ['red', 'blue', 'green', 'yellow']

# The function that gets passed to key must only accept a single argument.

def get_second_element(item):
    return item[1]

items = [(4, 1), (1, 2), (-9, 0))]
items.sort(key=get_second_element)
items  # [(-9, 0), (4, 1), (1, 2)]

# Random Choice of Element from a list
import random
mylist = ["apple", "banana", "cherry"]
print(random.choice(mylist))

# *********************************************************************

# DICTIONARY
# Each object in a dictionary has two parts: a key and a value

# dictionary literal
capitals = {
    "California": "Sacramento",
    "New York": "Albany",
    "Texas": "Austin",
}

# dict() built-in
key_value_pairs = (
    ("california", "Sacramento"),
    ("New York", "Albany"),
    ("Texas", "Austin"),
)
capitals = dict(key_value_pairs)
capitals  # {'California': 'Sacramento', 'New York': 'Albany', 'Texas': 'Austin'}

# empty dictionary
{}  # literal
dict()  # built-in

# Accesing Dictionary Values
capitals["Texas"]  # 'Austin'

# Adding and Removing Values in a Dictionary
capitals["Colorado"] = "Denver"  # {'California': 'Sacramento', 'New York': 'Albany', 'Texas': 'Austin', 'Colorado': 'Denver'}

# If a key is given a new value, Python overwrites the old one
capitals["Texas"] = "Houston"  # {'California': 'Sacramento', 'New York': 'Albany', 'Texas': 'Houston', 'Colorado': 'Denver'}

# Remove an item from dictionary
del capitals["Texas"]  # {'California': 'Sacramento', 'New York': 'Albany', 'Colorado': 'Denver'}

# Checking the Existence od Dictionary Keys
"Arizaona" in capitals  # False
"California" in capitals  # True

# *********************************************************************
# Iterating Over Dictionary
for state in capitals:
    print(f"The capital of {state} is {capitals[state]}")
    
# .items()
capitals.items()  # dict_items([('California', 'Sacramento'), ('New York', 'Albany'), ('Colorado', 'Denver')])
type(capitals.items())  # <class 'dict_items'>

for state, capital in capitals.items():
    print(f"The capital of {state} is {capital}")
    
# Dictionary Keys and Immutability
# Valid Dictionary Key Types:
# integers
# floats 
# strings
# booleans
# tuples

# Nested Dictionaries
states = {
    "California": {
        "capital": "Sacramento",
        "flower": "California Poppy"
    },
    "New York": {
        "cappital": "Albany",
        "flower": "Rose"
    },
    "Texas": {
        "capital": "Austin",
        "flower": "Bluebonnet"
    },
}

states["Texas"]  # {'capital': 'Austin', 'flower': 'Bluebonnet'}
states["Texas"]["flower"]  # 'Bluebonnet'

# ************************************************************************
# Use a list when:
# • Data has a natural order to it
# • You will need to update or alter the data during the program
# • The primary purpose of the data structure is iteration
# Use a tuple when:
# • Data has a natural order to it
# • You will not need to update or alter the data during the program
# • The primary purpose of the data structure is iteration
# Use a dictionary when:
# • The data is unordered, or the order does not matter
# • You will need to update or alter the data during the program
# • The primary purpose of the data structure is looking up values
# ************************************************************************



# str. 278