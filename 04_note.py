# PEP 8 style guide (https://pep8.org/)

# multiline string """multiline string"""

len('jakiś napis')  # długość stringa

# Concatenation
string = "string1" + "string2"
# Indexing
string[0]  # output: "s"
string[-1]  # output: "2" - ostatni znak w stringu
# Slicing
string[0:3]  # output: "str" (to samo [:3])

# Strings are immutable, which means that you can’t change them once you’ve created them.
>>> name = "Picard"
>>> name.upper()
'PICARD'
>>> name 
'Picard'

"Jean-luc Picard".lower()  # output: "jean-luc picard"
"Jean-luc Picard".upper()  # output: "JEAN-LUC PICARD"

# Removing Whitespace From a String
"    Jean-luc Picard    ".rstrip()  # output: "    Jean-luc Picard"
"    Jean-luc Picard    ".lstrip()  # output: "Jean-luc Picard    "
"    Jean-luc Picard    ".strip()  # output: "Jean-luc Picard"

# String Starts or Ends With a Particular String
"Enterprise".startswith("En")  # output: True ("en" - False)
"Enterprise".endswith("riSe")  # output: False ("rise" - True)

# User input
prompt = input("Say something loudly!")
loudly_prompt = prompt.upper()  # input: "Yeaaaa!"
print("I said:", loudly_prompt)  
# output: 'i said:YEAAAA!'

# Converting string to number
int()
float()

# Converting number to string
str()

# Formatted Strin Literals (f-string)
name = "Zaphod"
heads = 2
arms = 3
print(f"{name} has {heads} heads and {arms} arms.")
# output: 'Zaphod has 2 heads and 3 arms.'

# other techniques
print(name, "has", str(heads), "heads and", str(arms), "arms.")
print(name + " has " + str(heads) + " heads and " + str(arms) + " arms.")
print("{} has {} heads and {} arms.".format(name, heads, arms))

# find() returns index of the ONLY FIRST occurrence of the string (is case-sensitive) - if doesn't find it returns -1

# replace() replaces each instance of substring with another string
my_story = "I'm telling you the truth; nothing but the truth!"
my_story.replace("the truth", "lies")
# output: "I'm telling you lies; nothing but lies!"




# 105 strona