# 1. Write a script that asks the user to input a number and then displays that number rounded to two decimal places. When run, your program should look like this:
# Enter a number: 5.432
# 5.432 rounded to 2 decimal places is 5.43
# 2. Write a script that asks the user to input a number and then displays the absolute value of that number. When run, your program should look like this:
# Enter a number: -10
# The absolute value of -10 is 10.0
# 3. Write a script that asks the user to input two numbers by using the input() function twice, then display whether or not the diﬀerence between those two number is an integer. When run, your program should look like this:
# Enter a number: 1.5
# Enter another number: .5
# The difference between 1.5 and .5 is an integer? True!
# If the user inputs two numbers whose diﬀerence is not integral, the output should look like this:
# Enter a number: 1.5
# Enter another number: 1.0
# The difference between 1.5 and 1.0 is an integer? False!

num = float(input('Enter a number: '))
# ad1
print(f'{num} rounded to 2 decimal places is {round(num, 2)}')

# ad2
print(f'The absolute value of {num} is {abs(num)}')

# ad3
num2 = float(input('Enter another number: '))
print(f'The difference between {num} and {num2} is an integer? {(num - num2).is_integer()}!')