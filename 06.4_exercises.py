"""
1. Write a for loop that prints out the integers 2 through 10, each on
a new line, by using the range() function.
2. Use a while loop that prints out the integers 2 through 10 (Hint:
You’ll need to create a new integer ﬁrst.)
3. Write a function called doubles() that takes one number as its input
and doubles that number. Then use the doubles() function in a
loop to double the number 2 three times, displaying each result on
a separate line. Here is some sample output:
4
8
16
"""
# ad 1
print("# ex 1")
for n in range(2,11):
    print(n)
    
# ad 2
print("# ex 2")
num = 2

while num <= 10:
    print(num)
    num = num + 1

# ad 3
print("# ex 3")
def doubles(num2):
    return num2 * 2

num2 = 2
# index = 1

# while index <= 3:
#     num2 = doubles(num2)
#     print(num2)
#     index = index + 1

for n in range(1, 4):
    num2 = doubles(num2)
    print(num2)
     