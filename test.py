phrase = "X it marks the spot"

for character in phrase:
    if character == "X":
        print(" X ")
        break
else:
    print("There was no 'X' in the phrase")