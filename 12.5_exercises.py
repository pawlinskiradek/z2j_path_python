# 1. Write the following text to ﬁle called starships.txt in your home directory:
# Discovery
# Enterprise
# Defiant
# Voyager
# Each word should be on a separate line.
from pathlib import Path

path = Path.home() / "starships.txt"

list_of_starships = [
    "Discovery\n",
    "Enterprise\n",
    "Defiant\n",
    "Voyager\n",
]

with path.open(mode="w", encoding="utf-8") as file:
    file.writelines(list_of_starships)
    
# 2. Read the ﬁle starhips.txt you created in Exercise 1 and print each line of text in the ﬁle. The output should not have extra blank lines between each word.
with path.open(mode="r", encoding="utf-8") as file:
    # for starship in file.readlines():
    #     print(starship, end="")
    text = file.read()
print(text)
# 3. Read the ﬁle startships.txt and print the names of the starships that start with the letter D.
print("The names of the starships that start with the letter D:")
with path.open(mode="r", encoding="utf-8") as file:
    # starts_with_d = list(file.read())
    for line in file.readlines():
        if line.startswith("D"):
            print(line, end="")
