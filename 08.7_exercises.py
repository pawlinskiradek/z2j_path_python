"""
1. Write a function called roll() that uses the randint() function to
simulate rolling a fair die by returning a random integer between
1 and 6.
2. Write a script that simulates 10,000 rolls of a fair die and displays
the average number rolled.
"""

from random import randint

def roll():
    return randint(1, 6)

# print(roll())

# Simulate 10,000 rolls of a die and display the average number rolled.
total_rolled = 0

for rolling in range(10_000):
    total_rolled = total_rolled + roll()
    
print(total_rolled / 10_000)