# 1. Write a script that takes input from the user and displays that input back.
name = input("What is Your name? ")
print("Your name is ", name)
# 2. Write a script that takes input from the user and displays the input in lowercase.
print("Your name in lowercase: ", name.lower())
# 3. Write a script that takes input from the user and displays the number of characters inputted.
print("Number of letters in Your name is: ", len(name))