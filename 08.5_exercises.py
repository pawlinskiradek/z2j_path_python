"""
1. Using break, write a program that repeatedly asks the user for some
input and only quits if the user enters "q" or "Q".
2. Using continue, write a program that loops over the numbers 1 to
50 and prints all numbers that are not multiples of 3.
"""

# ad1
while True:
    text = input("Enter some input: ")
    if text.lower() == "q":
        break
print("Ufff...")  

# ad2
for n in range(1, 51):
    if n % 3 == 0:
        continue
    print(n)