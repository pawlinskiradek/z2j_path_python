import csv
from pathlib import Path

# Path to the input CSV file
scores_csv_path = (
    Path.cwd()
    / "practice_files"
    / "scores.csv"
)
# scores_csv_path = Path.home() / "scores.csv"

# Read data from the CSV file into a list of dictionaries
with scores_csv_path.open(mode="r", encoding="utf-8") as file:
    reader = csv.DictReader(file)
    scores = [row for row in reader]

# Dictionary to store the highest scores for each player
high_scores = {}

# Iterate through each row in the scores list
for item in scores:
    name = item["name"]
    score = int(item["score"])

    # If the name is not in high_scores, add it with the current score
    if name not in high_scores:
        high_scores[name] = score
    else:
        # If the current score is higher than the stored high score, update it
        if score > high_scores[name]:
            high_scores[name] = score

# The high_scores dictionary now contains each player's highest score

# Path to the output CSV file
# output_csv_file = Path.cwd() / "high_scores.csv"
output_csv_file = Path.home() / "high_scores.csv"

# Write the high scores to the new CSV file
with output_csv_file.open(mode="w", encoding="utf-8") as file:
    writer = csv.DictWriter(file, fieldnames=["name", "high_score"])
    writer.writeheader()

    # Iterate through the high_scores dictionary and write each player's name and highest score
    for name in high_scores:
        row_dict = {"name": name, "high_score": high_scores[name]}
        writer.writerow(row_dict)
