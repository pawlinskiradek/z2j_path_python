# Challenge: Wax Poetic
# In this challenge, you’ll write a program that generates poetry.
import random

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes",  "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant",  "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
adverbs =  ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]

def make_poem():
    # randomly select 3 nouns
    n1 = random.choice(nouns)
    n2 = random.choice(nouns)
    n3 = random.choice(nouns)
    # make sure that every noun is different
    while n1 == n2:
        n2 = random.choice(nouns)
    while n1 == n3 or n2 == n3:
        n3 = random.choice(nouns)
    
    # randomly select 3 verbs
    v1 = random.choice(verbs)
    v2 = random.choice(verbs)
    v3 = random.choice(verbs)
    # make sure that every verb is different
    while v1 == v2:
        v2 = random.choice(verbs)
    while v1 == v3 or v2 == v3:
        v3 = random.choice(verbs)
        
    # randomly select 3 adjectives
    adj1 = random.choice(adjectives)
    adj2 = random.choice(adjectives)
    adj3 = random.choice(adjectives)
    # make sure that every verb is different
    while adj1 == adj2:
        adj2 = random.choice(adjectives)
    while adj1 == adj3 or adj2 == adj3:
        adj3 = random.choice(adjectives)
    
    # randomly select 2 prepositions
    prep1 = random.choice(prepositions)
    prep2 = random.choice(prepositions)
    # make sure that every preposition is different
    while prep1 == prep2:
        prep2 = random.choice(prepositions)
    
    # randomly select 1 adverb
    adv1 = random.choice(adverbs)
    
    # check first letter of {adj1} to add an article
    if "aeiou".find(adj1[0]) != -1:
        article = "An"
    else:
        article = "A"
    
    # Create the poem
    poem = (
        f"{article} {adj1} {n1}\n\n"
        f"{article} {adj1} {n1} {v1} {prep1} the {adj2} {n2}\n"
        f"{adv1}, the {n1} {v2}\n"
        f"the {n2} {v3} {prep2} a {adj3} {n3}"
    )
    return poem

poem = make_poem()

print(poem)
