# Write a script called exponent.py that receives two numbers from the user and displays the ﬁrst number raised to the power of the second number.
# 1. Before you can do anything with the user’s input, you will have to assign both calls to input() to new variables.
# 2. The input() function returns a string, so you’ll need to convert the user’s input into numbers in order to do arithmetic.
# 3. You can use an f-string to print the result.
# 4. You can assume that the user will enter actual numbers as input.

base = float(input('Enter a base: '))
exponent = int(input('Enter an exponent: '))

print(f'{base} to the power of {exponent} = {base**exponent}')