# You have 100 cats.
# One day you decide to arrange all your cats in a giant circle. Initially, none of your cats have any hats on. You walk around the circle 100 times, always starting at the same spot, with the ﬁrst cat (cat # 1). Every time you stop at a cat, you either put a hat on it if it doesn’t have one on, or you take its hat oﬀ if it has one on.
# 1. The ﬁrst round, you stop at every cat, placing a hat on each one.
# 2. The second round, you only stop at every second cat (#2, #4, #6, #8, etc.).
# 3. The third round, you only stop at every third cat (#3, #6, #9, #12, etc.).
# 4. You continue this process until you’ve made 100 rounds around the cats (e.g., you only visit the 100th cat).
# Write a program that simply outputs which cats have hats at the end.

# Masz 100 kotów.
# Pewnego dnia postanawiasz ułożyć wszystkie swoje koty w gigantyczny okrąg. Początkowo żaden z twoich kotów nie nosi kapelusza. Okrążasz krąg 100 razy, zawsze zaczynając od tego samego miejsca, z pierwszym kotem (kot nr 1). Za każdym razem, gdy zatrzymujesz się przed kotem, albo zakładasz mu kapelusz, jeśli go nie ma, albo zdejmujesz kapelusz, jeśli go ma.
# 1. W pierwszej rundzie zatrzymujesz się przy każdym kocie i na każdym zakładasz kapelusz.
# 2. W drugiej rundzie zatrzymujesz się tylko na co drugim kocie (#2, #4, #6, #8 itd.).
# 3. W trzeciej rundzie zatrzymujesz się tylko na co trzecim kocie (#3, #6, #9, #12, itd.).
# 4. Kontynuujesz ten proces, aż wykonasz 100 rund wokół kotów (np. odwiedzisz tylko setnego kota).
# Napisz program, który po prostu wyświetli na końcu listę kotów, które mają kapelusze.

keys = list(range(1, 101))
values = ["no_hat"] * 100
cats = dict(zip(keys, values))  # generating 100 cats with the value "no hat"

round_around = 1

while round_around <= 100:
    for num_cat in cats:
        if num_cat % round_around == 0:
            if cats[num_cat] == "no_hat":
                cats[num_cat] = "hat_on"
            else:
                cats[num_cat] = "no_hat"
    round_around = round_around + 1
    
for keys, values in cats.items():
    if values == "hat_on":
        print(keys)

# ***************************************************************************
# SECOND SOLUTION
# ***************************************************************************

# 9.9 - Challenge: Cats With Hats
# Solution to challenge

# def get_cats_with_hats(array_of_cats):
#     cats_with_hats_on = []
#     # We want to walk around the circle 100 times
#     for num in range(1, 100 + 1):
#         # Each time we walk around, we visit 100 cats
#         for cat in range(1, 100 + 1):
#             # Determine whether to visit the cat
#             # Use modulo operator to visit every 2nd, 3rd, 4th,... etc.
#             if cat % num == 0:
#                 # Remove or add hat depending on
#                 # whether the cat already has one
#                 if array_of_cats[cat] is True:
#                     array_of_cats[cat] = False
#                 else:
#                     array_of_cats[cat] = True

#     # Add all number of each cat with a hat to list
#     for cat in range(1, 100 + 1):
#         if array_of_cats[cat] is True:
#             cats_with_hats_on.append(cat)

#     # Return the resulting list
#     return cats_with_hats_on


# # Cats contains whether each cat already has a hat on,
# # by default all are set to false since none have been visited
# cats = [False] * (100 + 1)
# print(get_cats_with_hats(cats))


# ***************************************************************************
# THIRD SOLUTION
# ***************************************************************************

# # 9.9 - Challenge: Cats With Hats
# # Alternative solution to challenge


# number_of_cats = 100
# cats_with_hats = []
# number_of_laps = 100

# # We want the laps to be from 1 to 100 instead of 0 to 99
# for lap in range(1, number_of_laps + 1):
#     for cat in range(1, number_of_cats + 1):

#         # Only look at cats that are divisible by the lap
#         if cat % lap == 0:
#             if cat in cats_with_hats:
#                 cats_with_hats.remove(cat)
#             else:
#                 cats_with_hats.append(cat)

# print(cats_with_hats)

# ***************************************************************************
# FOURTH SOLUTION
# ***************************************************************************

# # 9.9 - Challenge: Cats With Hats
# # Alternative solution to challenge using dictionaries


# theCats = {}

# # By default, no cats have been visited
# # so we set every cat's number to False
# for i in range(1, 101):
#     theCats[i] = False

# # Walk around the circle 100 times
# for i in range(1, 101):
#     # Visit all cats each time we do a lap
#     for cats, hats in theCats.items():
#         # Determine whether or not we visit a cat
#         if cats % i == 0:
#             # Add or remove the hat
#             if theCats[cats]:
#                 theCats[cats] = False
#             else:
#                 theCats[cats] = True

# # Print whether each cat has a hat
# for cats, hats in theCats.items():
#     if theCats[cats]:
#         print(f"Cat {cats} has a hat.")
#     else:
#         print(f"Cat {cats} is hatless!")