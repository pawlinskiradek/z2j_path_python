# Integers
# Integers literals can by written in two different ways:
1000000
1_000_000

# Floating-Point Numbers (float)
# E-notation 
200000000000000000.0
2e+17 # The + sign indicates that the exponent 17 is a positive number.
1e-4 >>> 0.0001 # The literal 1e-4 interpreted as 10 raised to the power -4
# which is 1/10000 or, equivalently 0.0001

# Division with the / operator always returns a float.
int(9 / 3) # to get an integer you can use int() to convert result
# output: 3

# Integer Division
9 // 3
# output: 3

# round() function, for rounding numbers to some number of decimal places
round(2.5) # 2
round(3.5) # 4 rounding ties to even

round(3.14159, 3) #3.142

# abs() function, for getting the absolute value of numbers
abs(3) # 3
abs(-5.0) # 5.0

# pow() function, for raising a number to some power
pow(2, 3) # 8
pow(2, -2) # 0.25

# pow(x, y, z) i equivalent to (x ** y) % z
pow(2, 3, 2) # 0
pow(2, 3, 3) # 2

# number method .is_integer() returns True if the number is itegral - meaning it has no fractional part - and return False otherwise.
num = 2.5
num.is_integer() # False

num = 2.0
num.is_integer() # True


# display numbers
n = 7.125
f"The value of n is {n:.2f}" # 'The value of n is 7.12'

# insert commas to group the integer part of large numbers
n = 1234567890
f"The value of n is {n:,}" # 'The value of n is 1,234,567,890'

n = 1234.56
f"The value of n is {n:,.2}" # 'The value of n is 1,234.56'

# % display percentages, multiplies a number by 100
ratio = 0.9
f"Over {ratio:.1%} of Pythonistas say 'Real Python rocks!'"
# Over 90.0% of Pythonistas say 'Real Python rocks!'

# https://docs.python.org/3/library/string.html#format-string-syntax_



# 134 strona