# 1. Create a string and print its length using the len() function.
string1 = "jakiś tekst"
lenght_of_string  = len(string1)
print(lenght_of_string)
# 2. Create two strings, concatenate them, and print the resulting string.
word1 = "kogiel"
word2 = "mogiel"
mixed_words = word1 + word2
print(mixed_words)
# 3. Create two strings and use concatenation to add a space inbetween them. Then print the result.
first_name = "Radek"
last_name = "Pawliński"
full_name = first_name + " " + last_name
print(full_name)
# 4. Print the string "zing" by using slice notation on the string "bazinga" to specify the correct range of characters.
word = "bazinga"
word = word[2:6]
print(word)