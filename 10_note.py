# Object-Oriented Programming (OOP)

# Define a Class
class Dog:
    pass  # place holder where code will evantually go

class Dog:
    # Class Attribute
    species = "Canis familiaris"
    
    def __init__(self, name, age):
        self.name = name
        self.age = age

# Instantiate an Object
a = Dog()
b = Dog()  # new Dog instance is located at a different memory address
a == b  # False

buddy = Dog("Buddy", 9)
miles = Dog("Miles", 4)

# Access using dot notation
buddy.name  # 'Buddy'
miles.age  # 4

# Class atributes are accessed the same way:
buddy.species  # 'Canis familiaris'

buddy.species == miles.species  # True

# Instances ane class attributes can be modified dynamically:
buddy.age = 10
buddy.age  # 10

miles.species = "Felis silvestris"
miles.species  # 'Felis silvestris'

# Instance Methods
# Instance methods are fuctions defined inside of a class

class Dog:
    # Class Attribute
    species = "Canis familiaris"
    
    def __init__(self, name, age):
        self.name = name
        self.age = age
    
    # Instance method
    def description(self):
        return f"{self.name} is {self.age} years old"
    
    # Another instance method
    def speak(self, sound):
        return f"{self.name} says {sound}"

miles.description()  # 'Miles is 4 years old'
miles.speak("Woof Woof")  # 'Miles says Woof Woof'

# dunder method (they begin and end with double underscores) - __str__

class Dog:
    # Class Attribute
    species = "Canis familiaris"
    
    def __init__(self, name, age):
        self.name = name
        self.age = age
    
    # Replace .description() with __str__()
    def __str__(self):
        return f"{self.name} is {self.age} years old"
    
    # Another instance method
    def speak(self, sound):
        return f"{self.name} says {sound}"
    
miles = Dog("Miles", 4)
print(miles)  # Miles is 4 years old

# Inherit From Other Classes
# Child classes can override and extend the attribites and methods of parent classes.

# The object Class
class Dog(object):
    pass
# In Pytho 3, this is the same as:
class Dog:
    pass

# Dog Park Example
class Dog:
    species = "Canis familiaris"
    
    def __init__(self, name, age, breed):
        self.name = name
        self.age = age
        self.breed = breed
    
    def speak(self, sound):
        return f"{self.name} says {sound}"

miles = Dog("Miles", 4, "Jack Russell Terrier")
buddy = Dog("Buddy", 9, "Dachshund")
jack = Dog("Jack", 3, "Bulldog")
jim = Dog("Jim", 5, "Bulldog")

buddy.speak("Yap")  # 'Buddy says Yap'
jim.speak("Woof")  # 'Jim says Woof'
jack.speak("Woof")  # 'Jack says Woof'

# Parent Classes vs Child Classes

class Dog:
    species = "Canis familiaris"
    
    def __init__(self, name, age):
        self.name = name
        self.age = age
        
    def __str__(self):
        return f"{self.name} is {self.age} years old"
    
    def speak(self, sound):
        return f"{self.name} barks: {sound}"

# Create three new chold classes of the Dog class:
class JackRussellTerrier(Dog):
    pass

class Dachshund(Dog):
    pass

class Bulldog(Dog):
    pass

miles = JackRussellTerrier("Miles", 4)
buddy = Dachshund("Buddy", 9)
jack = Bulldog("Jack", 3)
jim = Bulldog("Jim", 5)

type(miles)  # <class '__main__.JackRussellTerrier'>

# miles is an instance of the Dog class
isinstance(miles, Dog)  # True

# Extending the Funcionality of a Parent Class

# To override a method defined on the parent class, you define a method with the same name on the child class
class JackRussellTerrier(Dog):
    def speak(self, sound="Arf"):
        return f"{self.name} says {sound}"

miles = JackRussellTerrier("Miles", 4)
miles.speak()  # "Miles says Arf"

# you can still call .speak() with a different sound:
miles.speak("Grrr")  # 'Miles says Grrr'

# You can access the parent class from inside  method of a child class by using the super() function.
class JackRussellTerrier(Dog):
    def speak(self, sound="Arf"):
        return super().speak(sound)

miles = JackRussellTerrier("Miles", 4)
miles.speak()  # "Miles barks: Arf"

# -----------------------------------------------------------
# 10.3 exercise
class Rectangle:
    def __init__(self, length, width):
        self.length = length
        self.width = width
    def area(self):
        return self.length * self.width

# Square class inherits from the Rectangle class and that is instantiated with a single attribute called side_length.
class Square(Rectangle):
    def __init__(self, side_length):
        super().__init__(side_length, side_length)
    
square_test = Square(4)
print(square_test.area())

square_test.width = 5  # Modifies .width but not .length
print(square_test.area())
# -----------------------------------------------------------



# str. 301