# 1. Create a string containing an integer, then convert that string into an actual integer object using int(). Test that your new object is a number by multiplying it by another number and displaying the result.
number1 = "23"
number1 = int(number1)
print(number1 * 2)
# 2. Repeat the previous exercise, but use a floating-point number and float().
number1 = float(number1)
print(number1 *2)
# 3. Create a string object and an integer object, then display them side-by-side with a single print statement by using the str() function.
number2 = "23"
number3 = 21
print(number2 + " " + str(number3))
# 4. Write a script that gets two numbers from the user using the input() function twice, multiplies the numbers together, and displays the result. If the user enters 2 and 4, your program should print the following text: The product of 2 and 4 is 8.0.
num1 = input("Podaj pierwszą liczbę: ")
num2 = input("Podaj drugą liczbę: ")
result  = float(num1) * float(num2)
print("The product of "+str(num1)+" and "+str(num2)+" is "+str(result)+".")