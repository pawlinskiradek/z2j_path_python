# 1. Create a list named food with two elements "rice" and "beans".
food = ["rice", "beans"]
# 2. Append the string "broccoli" to food using .append().
food.append("broccoli")
# 3. Add the string "bread" and "pizza" to "food" using .extend().
food.extend(["bread", "pizza"])
# 4. Print the ﬁrst two items in the food list using print() and slicing
# notation.
print(food[:2])
# 5. Print the last item in food using print() and index notation.
print(food[len(food)-1])
print(food[-1])
# 6. Create a list called breakfast from the string "eggs, fruit, orange
# juice" using the string .split() method.
breakfast = "eggs, fruit, orange juice".split(", ")
print(breakfast)
# 7. Verify that breakfast has three items using len().
if len(breakfast) == 3:
    print("breakfast has 3 items.")
else:
    print("breakfast hasn't 3 items.")
# 8. Create a new list called lengths using a list comprehension that con-
# tains the lengths of each string in the breakfast list.
lenghts = [len(item) for item in breakfast]
print(lenghts)