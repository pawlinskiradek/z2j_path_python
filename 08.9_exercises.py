"""
Write a program that simulates the election 10,000 times and prints
the percentage of where Candidate A wins.
To keep things simple, assume that a candidate wins the election is
they win in at least two of the three regions.
"""

# import random

# def simulate_election():
#     region1_chance = 87
#     region2_chance = 65
#     region3_chance = 17
    
#     wins = 0
    
#     for _ in range(10000):
#         region1_result = random.randint(1, 100)
#         region2_result = random.randint(1, 100)
#         region3_result = random.randint(1, 100)
        
#         if region1_result <= region1_chance and region2_result <= region2_chance:
#             wins += 1
#         elif region1_result <= region1_chance and region3_result <= region3_chance:
#             wins += 1
#         elif region2_result <= region2_chance and region3_result <= region3_chance:
#             wins += 1
    
#     return wins / 100

# percentage_wins = simulate_election()
# print(f"Percentage of times Candidate A wins: {percentage_wins}%")

# ****************************************

# 8.9 - Challenge: Simulate an Election
# Solution to challenge


# Simulate the results of an election using a Monte Carlo simulation

from random import random

num_times_A_wins = 0
num_times_B_wins = 0

num_trials = 10_000
for trial in range(0, num_trials):
    votes_for_A = 0
    votes_for_B = 0

    # Determine who wins the 1st region
    if random() < 0.87:
        votes_for_A = votes_for_A + 1
    else:
        votes_for_B = votes_for_B + 1

    # Determine who wins the 2nd region
    if random() < 0.65:
        votes_for_A = votes_for_A + 1
    else:
        votes_for_B = votes_for_B + 1

    # Determine who wins the 3rd region
    if random() < 0.17:
        votes_for_A = votes_for_A + 1
    else:
        votes_for_B = votes_for_B + 1

    # Determine overall election outcome
    if votes_for_A > votes_for_B:
        num_times_A_wins = num_times_A_wins + 1
    else:
        num_times_B_wins = num_times_B_wins + 1

print(f"Probability A wins: {num_times_A_wins / num_trials}")
print(f"Probability B wins: {num_times_B_wins / num_trials}")