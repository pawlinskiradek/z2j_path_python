# 1. Create a new directory in your home folder called my_folder/.
from pathlib import Path

my_folder = Path.home() / "my_folder"
my_folder.mkdir(parents=True, exist_ok=True)
# print(my_folder.exists())

# 2. Inside my_folder/ create three ﬁles:
# • file1.txt
# • file2.txt
# • image1.png
paths = [
    my_folder / "file1.txt",
    my_folder / "file2.txt",
    my_folder / "image1.png"
]

for path in paths:
    path.touch()
    
# for path in my_folder.iterdir():
    # print(path)

# 3. Move the ﬁle image1.png to a new directory called images/ inside of
# the my_folder/ directory.
source = my_folder / "image1.png"
img_dir = my_folder / "images"
img_dir.mkdir(parents=True, exist_ok=True)
destination = img_dir / "image1.png"
source.replace(destination)

# for path in my_folder.iterdir():
#     print(path)

# 4. Delete the ﬁle file1.txt
file_path = my_folder / "file1.txt"
file_path.unlink()

# for path in my_folder.iterdir():
#     print(path)

# 5. Delete the my_folder/ directory.
import shutil
shutil.rmtree(my_folder)
print(my_folder.exists())