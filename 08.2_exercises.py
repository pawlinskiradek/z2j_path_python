# 1. Figure out what the result will be (True or False) when evaluating
# the following expressions, then type them into the interactive win-
# dow to check your answers:
(1 <= 1) and (1 != 1)  # False
not (1 != 2)  # False
("good" != "bad") or False  # True
("good" != "Good") and not (1 == 1)  # False
# 2. Add parentheses where necessary so that each of the following ex-
# pressions evaluates to True:
False == not True
True and False == True and False
not True and "A" == "B"

False == (not True)
(True and False) == (True and False)
not (True and ("A" == "B"))